const { Router } = require("express");
const { route } = require("express/lib/application");
const {
  getAllDocumentsDelete,
  getAllDocuments,
  getAllCarpeta,
  getAllUser,
  postUser,
  postFolder,
  postDocument,
  putUser,
  deleteUser,
  deleteFolder,
  deleteDocument,
  putFolder,
  putDocument,
  getUserLogin
} = require("../controlles/user.contoller");

const pool = require("../db");

const router = Router();

//obtener
router.get("/users", getAllUser);
router.get("/SistemaGD/:user/Carpetas", getAllCarpeta); 
router.get("/SistemaGD/:user/carpetas/:folder", getAllDocuments);
router.get("/SistemaGD/:user/:item", getAllDocumentsDelete);
//insertar
router.post("/SistemaGD/aggUser", postUser);
router.post("/SistemaGD/:user/carpetas", postFolder); 
router.post("/SistemaGD/:user/carpetas/:idFolder", postDocument); 
//actualizar
router.delete("/deleteUser/:id", deleteUser);
router.delete("/deleteFolder/:id", deleteFolder);
router.put("/deleteDocument/:id/:estado", deleteDocument);
//borrar
router.put("/putUser/:id", putUser);
router.put("/putFolder/:id", putFolder);
router.put("/putDoc/:id", putDocument);

router.get("/getUserLogin/:id/:pass", getUserLogin);

module.exports = router;
