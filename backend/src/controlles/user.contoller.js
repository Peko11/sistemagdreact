const pool = require("../db");
//METODOS PARA OBTENER INFO DE LA DATABASE
//obtener todos los usuarios del la BD
const getAllUser = async (req, res) => {
  try {
    const userList = await pool.query("select * from usuario");
    res.json(userList.rows);
  } catch (error) {
    console.log(error);
  }
};
//obtener todos las  de la BD
const getAllCarpeta = async (req, res) => {
  try {
    const carpetaList = await pool.query(
      `select Usuario,id_carpeta,id_carpeta2,nombre_carpeta  from usuario inner join carpeta on usuario.id_usuario=carpeta.id_usuario where usuario.Usuario='${req.params.user}'`
    );
    res.json(carpetaList.rows);
  } catch (error) {
    console.log(error);
  }
};
//obtener todos los documentos de la BD
const getAllDocuments = async (req, res) => {
  try {
    const documentList = await pool.query(
      `select id_doc,id_doc2,nombre_doc,id_carpeta2,estado from usuario 
      inner join carpeta on usuario.id_usuario=carpeta.id_usuario  
      inner join documento on documento.id_carpeta=carpeta.id_carpeta
      where usuario.Usuario='${req.params.user}' and id_carpeta2='${req.params.folder}'and documento.estado='activo';`
    );
    res.json(documentList.rows);
  } catch (error) {
    console.log(error);
  }
};
//obtener todos los documento eliminados de la BD
const getAllDocumentsDelete = async (req, res) => {
  try {
    const documentList = await pool.query(
      `select id_doc,id_doc2,nombre_doc,id_carpeta2,estado from usuario 
      inner join carpeta on usuario.id_usuario=carpeta.id_usuario  
      inner join documento on documento.id_carpeta=carpeta.id_carpeta
      where usuario.Usuario='${req.params.user}' and documento.estado='${req.params.item}';`
    );
    res.json(documentList.rows);
  } catch (error) {
    console.log(error);
  }
};
//METODOS DE INSERTAR
//insertar usuario
const postUser = async (req, res) => {
  const {
    id_usuario,
    Nombre_user,
    Correo_user,
    Usuario,
    rol_user,
    Clave_usuario,
  } = req.body;
  try {
    const result = await pool.query(
      "INSERT INTO  usuario (id_usuario,Nombre_user,Correo_user,Usuario,rol_user,Clave_usuario)VALUES ($1,$2,$3,$4,$5,$6) RETURNING *",
      [id_usuario, Nombre_user, Correo_user, Usuario, rol_user, Clave_usuario]
    );
    res.json(result.rows[0]);
  } catch (error) {
    console.log(error.message);
    res.json({ error: error.message });
  }
};
//insertar carpeta
const postFolder = async (req, res) => {
  const idUser = await pool.query(
    `select id_usuario from usuario where usuario.Usuario='${req.params.user}';`
  );
  const { id_carpeta2, nombre_carpeta } = req.body;
  try {
    const result = await pool.query(
      "INSERT INTO  carpeta (id_carpeta2,nombre_carpeta,id_usuario)VALUES ($1,$2,$3) RETURNING *",
      [id_carpeta2, nombre_carpeta, idUser.rows[0].id_usuario]
    );
    res.json(result.rows[0]);
  } catch (error) {
    console.log(error.message);
    res.json({ error: error.message });
  }
};
//insertar dcumento
const postDocument = async (req, res) => {
  let idfolder = await pool.query(`select id_carpeta
  from carpeta inner join usuario on usuario.id_usuario=carpeta.id_usuario
  where carpeta.id_carpeta2='${req.params.idFolder}' and usuario.Usuario='${req.params.user}';
  `);
  const { id_doc2, nombre_doc } = req.body;
  try {
    const result = await pool.query(
      "INSERT INTO  documento (id_doc2,nombre_doc,id_carpeta,estado)VALUES ($1,$2,$3,$4) RETURNING *",
      [id_doc2, nombre_doc, idfolder.rows[0].id_carpeta, "activo"]
    );
    res.json(result.rows[0]);
  } catch (error) {
    console.log(error.message);
    res.json({ error: error.message });
  }
};

//METODOS DE ACTUALIZAR
const putUser = async (req, res) => {
  const { id } = req.params;
  const { Nombre_user, Correo_user, Usuario, rol_user, Clave_usuario } =
    req.body;
  try {
    const result = await pool.query(
      "UPDATE usuario SET Nombre_user = $1,Correo_user = $2,Usuario = $3,rol_user = $4,Clave_usuario = $5 WHERE id_usuario = $6 returning*;",
      [Nombre_user, Correo_user, Usuario, rol_user, Clave_usuario, id]
    );
    res.json(result.rows[0]);
  } catch (error) {
    res.json({ error: error.message });
  }
};
const putFolder = async (req, res) => {
  const { id } = req.params;
  const { nombre_carpeta } = req.body;
  try {
    const result = await pool.query(
      "UPDATE carpeta SET nombre_carpeta = $1 WHERE id_carpeta = $2 returning*;",
      [nombre_carpeta, id]
    );
    res.json(result.rows[0]);
  } catch (error) {
    res.json({ error: error.message });
  }
};
const putDocument = async (req, res) => {
  const { id } = req.params;
  const { nombre_doc} = req.body;
  try {
    const result = await pool.query(
      "UPDATE documento SET nombre_doc = $1 WHERE id_doc = $2 returning*;",
      [nombre_doc, id]
    );
    res.json(result.rows[0]);
  } catch (error) {
    res.json({ error: error.message });
  }
};
//METODOS DE LOGIN
const getUserLogin = async (req, res) => {
  try {
    const userClave = await pool.query(
      `select * from usuario where usuario.Usuario='${req.params.id}' and usuario.Clave_usuario='${req.params.pass}';`
    );
    if(userClave.rows.length==0){
      res.json([]);
    }else{
      res.json(userClave.rows[0]);
    }
  } catch (error) {
    res.json(error);
  }
};
//METODOS DE ELIMINAR
const deleteUser = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await pool.query(
      `DELETE FROM usuario where usuario.id_usuario='${id}' RETURNING *;`
    );
    res.json(result.rows[0]);
  } catch (error) {
    res.json({ error: error.message });
  }
};
const deleteFolder = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await pool.query(
      `DELETE FROM carpeta where carpeta.id_carpeta2='${id}' RETURNING *;`
    );
    res.json(result.rows[0]);
  } catch (error) {
    res.json({ error: error.message });
  }
};
const deleteDocument = async (req, res) => {
  const { id,estado } = req.params;
  try {
    const result = await pool.query(
      "UPDATE documento SET estado =$1 WHERE id_doc2 = $2 returning*;",
      [estado,id]);
    res.json(result.rows[0]);
  } catch (error) {
    res.json({ error: error.message });
  }
};


module.exports = {
  getAllUser,
  postUser,
  deleteUser,
  putUser,
  getAllCarpeta,
  postFolder,
  getAllDocuments,
  postDocument,
  getAllDocumentsDelete,
  getUserLogin,
  deleteFolder,
  deleteDocument,
  putFolder,
  putDocument,
};
