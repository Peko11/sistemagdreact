create table usuario(
	id_usuario varchar(10) NOT NULL,
	Nombre_user varchar(50) NOT NULL,
	Correo_user varchar NOT NULL,
	Usuario varchar (12) not null,
	rol_user varchar (15) not null,
	Clave_usuario varchar(10) not null,
	CONSTRAINT id_usuario_pk PRIMARY KEY (id_usuario)
);

create table carpeta(
	id_carpeta serial,
	id_carpeta2 varchar(10) NOT NULL,
	nombre_carpeta varchar(50) NOT NULL,
	id_usuario varchar(10) NOT NULL,
	CONSTRAINT id_carpeta_pk PRIMARY KEY (id_carpeta),
	CONSTRAINT id_usuario_fk FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario) MATCH FULL ON DELETE RESTRICT ON UPDATE CASCADE
);

create table documento(
	id_doc serial,
	id_doc2 varchar(10) NOT NULL,
	nombre_doc varchar(50) NOT NULL,
	id_carpeta varchar(10) NOT NULL,
	estado varchar(12) not null,
	CONSTRAINT id_doc_pk PRIMARY KEY (id_doc),
	CONSTRAINT id_carpeta_fk FOREIGN KEY (id_carpeta) REFERENCES carpeta(id_carpeta) MATCH FULL ON DELETE RESTRICT ON UPDATE CASCADE
);

 
 --consulta de carpetas
 --select Usuario,id_carpeta,id_carpeta2,nombre_carpeta  from usuario inner join carpeta on usuario.id_usuario=carpeta.id_usuario where usuario.Usuario='${req.params.user}`
--consulta de los documentos
--select id_doc,nombre_doc,id_carpeta2,estado from usuario inner join carpeta on usuario.id_usuario=carpeta.id_usuario inner join documento on documento.id_carpeta=carpeta.id_carpeta where usuario.Usuario='${req.params.user}' and id_carpeta2='${req.params.folder}' ;