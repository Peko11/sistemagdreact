const express = require('express');
const morgan=require('morgan');
const bodyParser = require('body-parser');
const userRoutes= require("./routes/rutas.routes");
const cors =require('cors');
const app= express();

app.use(cors());
const port=process.env.PORT || 4000;
app.use(morgan('dev'));

app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json());
app.use(userRoutes);
app.listen(port);
console.log("server port 4000");