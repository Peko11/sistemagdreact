import "./cssComponentes/Registro.css";
import { Button, Modal, ModalFooter, ModalHeader } from "reactstrap";
import { useNavigate } from "react-router-dom";

export default function ModalEliminar(props) {
  const deletes = async (id, tipo) => {
    await fetch(`https://sistemagd.herokuapp.com/${tipo}/${id}`, {
      method: "DELETE",
    });
    if (props.accion == "Folders") {
      props.setFolder(
        props.folder.filter((folder) => folder.id_carpeta2 !== id)
      );
    } else {
      props.setData(props.datas.filter((data) => data.id_usuario !== id));
    }
  };
  const updates = async (id, estado) => {
    if (estado == "activo") {
      if (props.DeleArchiv == "archivar") {
        estado = "archivado";
      } else {
        estado = "eliminado";
      }
    } else {
      estado = "activo";
    }

    await fetch(`https://sistemagd.herokuapp.com/deleteDocument/${id}/${estado}`, {
      method: "PUT",
      Headers: {
        "Content-Type": "application/json",
      },

    });
    props.setDoc(props.Doc.filter((doc) => doc.id_doc2 !== id));
  };

  return (
    <>
      <Modal isOpen={props.estado}>
        <ModalHeader>
          ¿Está seguro que desea {props.DeleArchiv} {props.description}
        </ModalHeader>
        <ModalFooter>
          <Button
            onClick={() => {
              switch (props.accion) {
                case "Users":
                  deletes(props.id, "deleteUser");
                  break;
                case "Folders":
                  deletes(props.id, "deleteFolder");
                  break;
                case "Documents":
                  updates(props.id, props.estado2);
                  break;
              }

              props.setestado(false);
            }}
            color="btn btn-danger"
          >
            Eliminar
          </Button>
          <Button
            onClick={() => props.setestado(false)}
            color="btn btn-success"
          >
            {" "}
            Cerrar{" "}
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
}
