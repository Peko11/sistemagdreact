import React from "react";
import { Link } from "react-router-dom";
import { useMatch } from "react-router-dom";
export default function BarraIzquierda() {
  return (
    <header className="header-menu">
      <ul>
        <li>
          <Link to={"Carpetas"}>Carpetas Virtuales</Link>
        </li>
        <li>
          <Link to={"eliminado"}>Documentos Eliminados</Link>
        </li>
        <li>
          <Link to={"archivado"}>Documentos Archivados</Link>
        </li>
        <li>
          <Link to={"Administrar-usuario"}>Administrar Usuarios</Link>
        </li>
      </ul>
    </header>
  );
}
