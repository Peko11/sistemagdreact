import "./cssComponentes/busqueda.css";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { useState, useEffect } from "react";
import { Carpeta } from "./Carpeta";
import ModalEliminar from "./ModalEliminar";
import { useMatch } from "react-router-dom";
export default function VirtualFolder() {
  let match = useMatch("SistemaGD/:user/Carpetas");
  //hook para el modal de eliminar
  const [open3, setopen3] = useState(false);
  //hook para el modal de crear carpeta
  const [modalCrear, setmodalCrear] = useState(false);
  //hook para el id que se elimina
  const [IdDelete, setIdDelete] = useState();
  const [folder, setFolder] = useState([]);
  //hook para cambiar la data 'folder'
  const [folders, setfolder] = useState({
    id_carpeta2: "",
    nombre_carpeta: "",
  });
  //importamos la data 'folder'
  const loadFolder = async () => {
    const response = await fetch(
      "https://sistemagd.herokuapp.com/SistemaGD/" +
        match.params.user +
        "/Carpetas"
    );
    const data = await response.json();
    setFolder(data);
  };

  useEffect(() => {
    loadFolder();
  }, []);

  const handleChange = (e) => {
    setfolder({
      ...folders,
      [e.target.name]: e.target.value,
      id_carpeta2: "folder-" + (folder.length + 1),
    });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch(
      "https://sistemagd.herokuapp.com/SistemaGD/" +
        match.params.user +
        "/Carpetas",
      {
        method: "POST",
        body: JSON.stringify(folders),
        headers: { "Content-Type": "application/json" },
      }
    );
    const data = await res.json();
    loadFolder();
  };

  return (
    <>
      <div className="busquedaContainer">
        <Modal isOpen={modalCrear}>
          <ModalHeader>Crear Carpeta</ModalHeader>
          <ModalBody>
            <form onSubmit={handleSubmit}>
              <input
              id='inputName'
                required
                onChange={handleChange}
                placeholder="Nombre Carpeta"
                type="text"
                name="nombre_carpeta"
              />
              .
              <ModalFooter>
                <Button
                  onClick={() => {
                    if(document.getElementById('inputName').value!=''){
                      setmodalCrear(false);
                    }
                  }}
                  type="submit"
                  color="primary"
                >
                  Crear
                </Button>
                <Button
                  onClick={() => {
                    setmodalCrear(false);
                  }}
                  color="danger"
                >
                  Cerrar
                </Button>
              </ModalFooter>
            </form>
          </ModalBody>
        </Modal>

        <section className="virtualFolders">
          <section id="folders" className="folders">
            {folder?.map((elemento) => (
              <Carpeta
                open3={open3}
                setopen3={setopen3}
                IdDelete={IdDelete}
                setIdDelete={setIdDelete}
                id={elemento.id_carpeta2}
                key={elemento.id_carpeta}
                nombre={elemento.nombre_carpeta}
                usuario={elemento.id_usuario}
                tipo="folder"
                accion="delete"
              ></Carpeta>
            ))}
          </section>
        </section>
        <ModalFooter className="modalFooter">
          <Button onClick={() => setmodalCrear(true)} color="primary">
            Crear Carpeta
          </Button>
        </ModalFooter>
      </div>
      <ModalEliminar
        folder={folder}
        setFolder={setFolder}
        id={IdDelete}
        description="eliminar la Carpeta?"
        accion="Folders"
        estado={open3}
        setestado={setopen3}
      />
    </>
  );
}
