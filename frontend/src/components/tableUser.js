import "bootstrap/dist/css/bootstrap.css";
import "./cssComponentes/Registro.css";
import { Button } from "reactstrap";
import ModalEliminar from "./ModalEliminar";
import { useState } from "react/cjs/react.development";

export default function TableUser(props) {
  const [open3, setopen3] = useState(false);
  const [id, setid] = useState("");

  return (
    <>
      <table id="resultado">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Usuario</th>
            <th>Rol</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <tr id="U0">
            <td>U0</td>
            <td>Romina</td>
            <td>info@romina.com</td>
            <td>roma</td>
            <td>Admin</td>
            <td>
              <Button color="success">Editar</Button>
            </td>
          </tr>
          {props.datas.map((elemento) => (
            <tr key={elemento.id_usuario} id={elemento.id_usuario}>
              <td>{elemento.id_usuario}</td>
              <td>{elemento.nombre_user}</td>
              <td>{elemento.correo_user}</td>
              <td>{elemento.usuario}</td>
              <td>{elemento.rol_user}</td>
              <td>
                <Button
                  onClick={() => props.setopen(!props.open)}
                  color="btn btn-success"
                >
                  Editar
                </Button>
                .
                <Button
                  onClick={() => {
                    setid(elemento.id_usuario);
                    setopen3(!open3);
                  }}
                  color="btn btn-danger"
                >
                  Eliminar
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <ModalEliminar
        setData={props.setData}
        datas={props.datas}
        description="el registro?"
        accion="Users"
        id={id}
        estado={open3}
        setestado={setopen3}
      />
    </>
  );
}
