import React from "react";
import { FiFolder } from "react-icons/fi";
import { AiOutlineDelete, AiFillEdit } from "react-icons/ai";
import { Link } from "react-router-dom";
import { FcDocument } from "react-icons/fc";
import { VscDebugRestart } from "react-icons/vsc";
import { BiArchiveIn } from "react-icons/bi";

export const Carpeta = (props) => {
  const verfficar = () => {
    switch (props.tipo) {
      case "folder":
        return <FiFolder className="iconF"></FiFolder>;
      case "document":
        return <FcDocument className="iconF"></FcDocument>;
    }
  };
  const verificarAction = () => {
    if (props.tipo == "document") {
      switch (props.estado) {
        case "activo":
          return (
            <>
              <div
                onClick={() => {
                  props.setIdDelete(props.id);
                  props.setopen3(true);
                  props.setDeleArchiv('eliminar');
                }}
                className="iconDelete"
              >
                <AiOutlineDelete></AiOutlineDelete>
              </div>
              <div
                onClick={() => {
                  props.setIdDelete(props.id);
                  props.setopen3(true);
                  props.setDeleArchiv('archivar');

                }}
                className="iconArchiv"
              >
                <BiArchiveIn></BiArchiveIn>
              </div>
            </>
          );
        case "eliminado":
          return (
            <div
              onClick={() => {
                props.setIdDelete(props.id);
                props.setopen3(true);
              }}
              className="iconDelete"
            >
              <VscDebugRestart></VscDebugRestart>
            </div>
          );
        case "archivado":
          return (
            <div
              onClick={() => {
                props.setIdDelete(props.id);
                props.setopen3(true);
              }}
              className="iconDelete"
            >
              <VscDebugRestart></VscDebugRestart>
            </div>
          );
          break;
      }
    } else {
      return (
        <div
          onClick={() => {
            props.setIdDelete(props.id);
            props.setopen3(true);
          }}
          className="iconDelete"
        >
          <AiOutlineDelete></AiOutlineDelete>
        </div>
      );
    }
  };
  return (
    <>
      <div key={props.key} id={props.id} className="folder">
        <Link className="carpetaInfo" to={props.id}>
          {verfficar()}
          <p className="folderName">{props.nombre}</p>
        </Link>
        {verificarAction()}
      </div>
    </>
  );
};
