import React from "react";
import { useMatch } from "react-router-dom";
import { Carpeta } from "./Carpeta";
import { useState, useEffect } from "react";
import ModalEliminar from "./ModalEliminar";
import { ModalFooter, Button } from "reactstrap";
import { ModalBody, Modal, ModalHeader } from "reactstrap";
export const FolderInto = () => {
  let match = useMatch("SistemaGD/:user/Carpetas/:item");
  const [open3, setopen3] = useState(false); //hook para abrir el modal de eliminar
  const [CrearDoc, setCrearDoc] = useState(false); //hook para abrir el modal de agregar documento
  const [IdDelete, setIdDelete] = useState(); //hook para eliminar
  const [document, setDocument] = useState([]); //hook para obtener los documentos de la bd
  const [DeleArchiv, setDeleArchiv] = useState(""); //hook para deletear o archivar
  const loadDocument = async () => {
    const response = await fetch(
      "https://sistemagd.herokuapp.com/SistemaGD/" +
        match.params.user +
        "/Carpetas/" +
        match.params.item
    );
    const data = await response.json();
    setDocument(data);
    console.log(data);
  };
  useEffect(() => {
    loadDocument();
  }, []);
  const [Documento, setDocumento] = useState({
    id_doc2: "",
    nombre_doc: "",
  });
  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch(
      "https://sistemagd.herokuapp.com/SistemaGD/" +
        match.params.user +
        "/Carpetas/" +
        match.params.item,
      {
        method: "POST",
        body: JSON.stringify(Documento),
        headers: { "Content-Type": "application/json" },
      }
    );
    const data = await res.json();
    loadDocument();
  };
  const handleChangue = (e) => {
    setDocumento({
      ...Documento,
      [e.target.name]: e.target.value,
      id_doc2: "doc-" + (document.length + 1),
    });
  };

  return (
    <>
      <section className="virtualFolders">
        <section id="folders" className="folders">
          {document.map((elemento) => (
            <Carpeta
              setDeleArchiv={setDeleArchiv}
              open3={open3}
              setopen3={setopen3}
              IdDelete={IdDelete}
              setIdDelete={setIdDelete}
              id={elemento.id_doc2}
              key={elemento.id_doc}
              nombre={elemento.nombre_doc}
              tipo="document"
              estado={elemento.estado}
            ></Carpeta>
          ))}
        </section>
      </section>
      <ModalFooter className="modalFooter">
        <Button
          onClick={() => setCrearDoc(true)}
          className="botones"
          color="success"
        >
          Subir Docuemento
        </Button>{" "}
      </ModalFooter>
      <Modal isOpen={CrearDoc}>
        <ModalHeader>Subida de documentos</ModalHeader>
        <ModalBody>
          <form onSubmit={handleSubmit}>
            <label className="etiqueta">Nombre del documento</label>
            <input
              id="docc"
              required
              onChange={handleChangue}
              className="campos"
              type="text"
              name="nombre_doc"
            />
            .
            <ModalFooter>
              <Button
                onClick={() => {
                  if (document.getElementById('docc').value != '') {
                    setCrearDoc(false);
                  }
                }}
                color="primary"
                name="save"
                type="submit"
              >
                Guadar
              </Button>
              <Button
                  onClick={() => {
                    setCrearDoc(false);
                  }}
                  color="danger"
                >
                  Cerrar
                </Button>
            </ModalFooter>
          </form>
        </ModalBody>
      </Modal>

      <ModalEliminar
        Doc={document}
        setDoc={setDocument}
        DeleArchiv={DeleArchiv}
        id={IdDelete}
        estado2="activo"
        description="el documento?"
        accion="Documents"
        estado={open3}
        setestado={setopen3}
      />
    </>
  );
};
