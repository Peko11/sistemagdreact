import "./cssComponentes/Login.css";
import { Button } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Cookies from "universal-cookie";

export default function LoginUser() {
  const cookie = new Cookies();
  const navigate = useNavigate();
  const [login, SetLogin] = useState({
    usuario: "",
    contrasena: "",
  });

  useEffect(() => {
    ComponentDidMount();
  }, []);
  const ComponentDidMount = () => {
    const suser = cookie.get("usuario");
    if (suser) {
      navigate("/SistemaGD/" + suser);
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch(
      `https://sistemagd.herokuapp.com/getUserLogin/${login.usuario}/${login.contrasena}`
    );
    const data = await res.json();
    console.log(data.length);
    if (data.length != 0) {
      cookie.set("id_usuario", data.id_usuario, { path: "/" });
      cookie.set("nombre_user", data.nombre_user, { path: "/" });
      cookie.set("correo_user", data.correo_user, { path: "/" });
      cookie.set("usuario", data.usuario, { path: "/" });
      cookie.set("rol_user", data.rol_user, { path: "/" });
      alert("Bienvenido " + data.nombre_user);
      navigate("/SistemaGD/" + data.usuario);
    } else {
      alert("DATOS INCORRECTOS!!!");
    }
  };
  const handleChangue = (e) => {
    SetLogin({ ...login, [e.target.name]: e.target.value });
  };
  //

  return (
    <div className="containerForm">
      <form onSubmit={handleSubmit} className="form-login">
        <h2>LOGIN</h2>
        <input
          onChange={handleChangue}
          className="control"
          type="text"
          name="usuario"
          placeholder="Usuario"
          required
        />
        <input
          onChange={handleChangue}
          className="control"
          type="password"
          name="contrasena"
          placeholder="Contraseña"
          required
        />
        <Button id="btn" color="success" type="submit">
          Ingresar
        </Button>
      </form>
    </div>
  );
}
