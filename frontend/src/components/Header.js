import { FiLogOut } from "react-icons/fi";
import Cookies from "universal-cookie";
function Cabecera(props) {
  const cookie = new Cookies();
  const salirUser = () => {
    cookie.remove('id_usuario',{path:"/"});
    cookie.remove('nombre_user',{path:"/"});
    cookie.remove('correo_user',{path:"/"});
    cookie.remove('usuario',{path:"/"});
    cookie.remove('rol_user',{path:"/"});  
    alert("Hasta luego ");
  };
  return (
    <div className="main-menu">
      <a className="linkLogin" href={`/SistemaGD/${props.user}/inicio`}>
        <h1 className="titulos">SISTEMA DE G.D</h1>
      </a>
      <a className="linkLogin">
        <h1 className="titulos">{props.user}</h1>
      </a>
      <a className="linkLogin" href="/Login">
        <FiLogOut
          onClick={() => {
            salirUser();
          }}
          className="logoSalir"
        ></FiLogOut>
      </a>
    </div>
  );
}
export default Cabecera;
