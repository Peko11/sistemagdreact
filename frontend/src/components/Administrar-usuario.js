import { Button } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import "./cssComponentes/Registro.css";
import { useState, useEffect } from "react";
import ModalRegistro from "./ModalRegistro";
import TableUser from "./tableUser";
export default function Administrar_usuario() {
  const [open, setopen] = useState(false);
  const [updateUser, setUpdateUser] = useState(false);
  const [datas, setData] = useState([]);
  const loadUser = async () => {
    const response = await fetch(
      "https://sistemagd.herokuapp.com/users"
    );
    const result = await response.json();
    setData(result);
  };

  useEffect(() => {
    loadUser();
  }, []);
  return (
    <>
      <section id="container hola">
        <div className="contentTitle">
          <h1 className="titlePag">Control de usuarios</h1>
          <Button
            className="btnCreateUser"
            color="primary"
            onClick={() => setopen(!open)}
          >
            Crear usuario
          </Button>
        </div>
        <TableUser
          setData={setData}
          datas={datas}
          updateUser={updateUser}
          open={open}
          setopen={setopen}
        ></TableUser>
      </section>
      <ModalRegistro
        setData={setData}
        datas={datas}
        estado={open}
        setestado={setopen}
        Titulo="Registrar Usuario"
        setUpdateUser={setUpdateUser}
      />
    </>
  );
}
