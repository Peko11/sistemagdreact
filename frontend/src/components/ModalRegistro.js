import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import { useState } from "react";
export default function ModalRegistro(props) {
  const [User, setUser] = useState({
    id_usuario: "",
    Nombre_user: "",
    Correo_user: "",
    Usuario: "",
    rol_user: "Secretario/a",
    Clave_usuario: "",
  }); //hook para cambiar el usuario
  const handleChange = (e) => {
    setUser({
      ...User,
      [e.target.name]: e.target.value,
      id_usuario: "U" + (props.datas.length + 1),
    });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch(
      "https://sistemagd.herokuapp.com/SistemaGD/aggUser",
      {
        method: "POST",
        body: JSON.stringify(User),
        headers: {
          "Content-type": "application/json",
        },
      }
    );
    const data = await res.json();
    props.setData([...props.datas, data]);
  };

  const validar = () => {
    if (
      document.getElementById('nombre').value != '' &&
      document.getElementById('correo').value != '' &&
      document.getElementById('usuario').value != '' &&
      document.getElementById('clave').value != ''
    ){props.setestado(false);}
      
  };
  return (
    <>
      <Modal isOpen={props.estado}>
        <ModalBody>
          <ModalHeader> Registrar Usuario</ModalHeader>

          <form onSubmit={handleSubmit}>
            <label>ID</label>
            <input
              readOnly
              name="Id_usuario"
              type="text"
              disabled
              value={"U" + props.datas.length}
            />
            <label>Nombre</label>
            <input
              required
              onChange={handleChange}
              name="Nombre_user"
              type="text"
              id="nombre"
              placeholder="Nombre completo"
            />

            <label>Correo electrónico</label>
            <input
              required
              onChange={handleChange}
              name="Correo_user"
              type="email"
              id="correo"
              placeholder="Correo electrónico"
            />

            <label>Usuario</label>
            <input
              required
              onChange={handleChange}
              name="Usuario"
              type="text"
              id="usuario"
              placeholder="Usuario"
            />

            <label>Clave</label>
            <input
              required
              onChange={handleChange}
              name="Clave_usuario"
              type="password"
              id="clave"
              placeholder="Clave de acceso"
            />
            <label>.</label>
            <ModalFooter>
              <Button
                color="success"
                onClick={() =>
                  validar()
                }
                type="submit"
              >
                Registrar
              </Button>
              <Button color="danger" onClick={() => props.setestado(false)}>
                Cerrar
              </Button>
            </ModalFooter>
          </form>
        </ModalBody>
      </Modal>
    </>
  );
}
