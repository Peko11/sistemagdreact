import React from "react";
import { useEffect, useState } from "react";
import { useMatch } from "react-router-dom";
import { Carpeta } from "./Carpeta";
import ModalEliminar from "./ModalEliminar";

export const DocArchivados = (props) => {
    const match= useMatch("SistemaGD/:user/:item");
    const [open3, setopen3] = useState(false);
    const [IdDelete, setIdDelete] = useState(false);
    const [doc, setDoc] = useState([]);
    const loadDoc = async () => {
      const response = await fetch(
        `https://sistemagd.herokuapp.com/SistemaGD/${match.params.user}/archivado`
      );
      const result = await response.json();
      setDoc(result)
    };
    useEffect(() => {
      loadDoc();
    }, []);
    
    return (
      <>
        <h1>Documentos Archivados</h1>
        <section className="virtualFolders">
          <section id="folders" className="folders">
            {doc.map((elemento) => (
              <Carpeta
                open3={open3}
                setopen3={setopen3}
                IdDelete={IdDelete}
                setIdDelete={setIdDelete}
                id={elemento.id_doc2}
                key={elemento.id_doc}
                nombre={elemento.nombre_doc}
                tipo="document"
                estado="archivado"
              ></Carpeta>
            ))}
          </section>
        </section>
        <ModalEliminar
          id={IdDelete}
          description="desarchivar el documento?"
          estado2='archivado'
          Doc={doc}
          setDoc={setDoc}
          estado={open3}
          setestado={setopen3}
          accion="Documents"
        />
      </>
    );
}
