import "./cssComponentes/HeaderFooter.css";
import Cabecera from "./Header";
import BarraIzquierda from "./BarraIzquierda";
import { Routes, Route} from "react-router-dom";
import Administrar_usuario from "./Administrar-usuario";
import VirtualFolder from "./virtualForder";
import { DocEliminados } from "./DocEliminados";
import { InicioGD } from "./InicioGD";
import { FolderInto } from "./FolderInto";
import { DocArchivados } from "./DocArchivados";
import Cookies from "universal-cookie";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
const cookie = new Cookies();
export default function PagePrincipal() {
  const navigate = useNavigate();
  useEffect(() => {
    ComponentDidMount();
  }, []);
  const ComponentDidMount = () => {
    const suser = cookie.get("usuario");
    if (!suser) {
      navigate("/Login");
    }
  };

  return (
    <>
      <div className="content-header">
        <Cabecera user={cookie.get("usuario")} />
      </div>
      <div className="bodyIndex">
        <BarraIzquierda />
        <div className="derecha">
          <Routes>
            <Route exact path={"/SistemaGD/:user"} element={<InicioGD />} />
            <Route path={"Carpetas"} element={<VirtualFolder />} />
            <Route path="Carpetas/:id" element={<FolderInto />} />
            <Route path="eliminado" element={<DocEliminados />} />
            <Route path="archivado" element={<DocArchivados />} />
            <Route
              path="Administrar-usuario"
              element={<Administrar_usuario />}
            />
          </Routes>
        </div>
      </div>
    </>
  );
}
